<?php

namespace Trespixelsmedia\PixelsAndCookies;

use Illuminate\Support\ServiceProvider;

class PixelsAndCookiesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadViewsFrom(__DIR__.'/views', 'pixels-and-cookies-package');


        $this->publishes([
            __DIR__.'/helpers/' => app_path('/Helpers/PixelsAndCookies'),
            __DIR__.'/models/' => app_path('/PixelsAndCookies'),
            __DIR__.'/controllers/' => app_path('Http/Controllers'),
            __DIR__.'/seeds/' => database_path('seeds/pixels-and-cookies'),
            __DIR__.'/assets/pixels-and-cookies-theme/' => public_path('vendor/pixels-and-cookies-theme'),
            __DIR__.'/resources/lang/es/' => resource_path('lang/es/'),
            __DIR__.'/resources/lang/en/' => resource_path('lang/en/'),
        ],'pixels-and-cookies-essential-files');


        /*
        * Optionals
        */
        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations/pixels-and-cookies'),
        ], 'pixels-and-cookies-migrations');

        $this->publishes([
            __DIR__.'/views/' => resource_path('views/vendor/pixels-and-cookies-package'),
        ], 'pixels-and-cookies-views');

        $this->publishes([
            __DIR__.'/helpers/' => app_path('/Helpers/PixelsAndCookies'),
        ], 'pixels-and-cookies-helpers');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
