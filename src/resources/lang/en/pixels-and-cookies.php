<?php

return [
	'text' => '<p class="gray-text">Lorem ipsum dol Lorem ipsum dol Lorem ipsum dol Lorem ipsum dol <a href="#" class="terms-link">Privacy Policies</a>. Lorem ipsum dol Lorem ipsum dol Lorem ipsum dol Lorem ipsum dol <a href="#" class="terms-link">Cookies</a>. Lorem ipsum dol Lorem ipsum dol Lorem ipsum dol Lorem ipsum dol <a href="#" class="terms-link">Terms and Conditions</a>.</p>',

	'customise-section-title' => '<h4 id="cookie-type-title">Select cookies to accept</h4>',

	'btn-accept' => 'Accept',

	'btn-customise' => 'Customise',
];