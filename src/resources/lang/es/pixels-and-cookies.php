<?php

return [
	'text' => '<p class="gray-text">Usamos cookies en nuestro sitio web. Las cookies se utilizan para mejorar la funcionalidad y el uso de nuestro sitio de Internet, como también para propósitos de análisis y publicidad. Para aprender más acerca de las cookies, cómo usarlas y cómo cambiar su configuración de cookies, conozca más <a href="/declaracion-cookies/" class="terms-link">aquí</a>. Si usa este sitio sin cambiar la configuración ya establecida, significa que está de acuerdo con nuestro uso de cookies.</p>',

	'customise-section-title' => '<h4 id="cookie-type-title">Seleccione cookies para aceptar</h4>',

	'btn-accept' => 'Aceptar',

	'btn-customise' => 'Personalizar',

		'declaration-text' => '<h1>Declaración de Cookies</h1>

	<section>
		<h3>Cookies</h3>

		<p>Para que este sitio funcione adecuadamente, instalamos en los dispositivos de los usuarios pequeños archivos de datos, conocidos como cookies. Usted puede ordenar a su navegador que rechace todas las cookies o que le avise cuando se envía una cookie. Sin embargo, si no acepta cookies, es posible que no pueda utilizar algunas partes de nuestro Servicio.</p>
	</section>

	<section>
		<h3>¿Qué son las cookies?</h3>

		<p>Una cookie es un pequeño archivo de texto que los sitios web instalan en el ordenador o el dispositivo móvil de los usuarios que los visitan. Las cookies hacen posible que el sitio web recuerde las acciones y preferencias del usuario (identificador de inicio de sesión, idioma, tamaño de letra y otras preferencias de visualización), para que este no tenga que volver a configurarlos cuando regrese al sitio o navegue por sus páginas.</p>
	</section>

	<section>
		<h3>Cómo utilizamos las cookies y sus clasificaciones</h3>

		<p>
			<span class="bold">1) Estrictamente necesarias: </span> Estas cookies son esenciales para permitirle navegar por el sitio web y utilizar sus características. Sin estas cookies, no podemos ofrecer nuestros servicios.
		</p>

		<table class="cookies-table">
			<tr>
				<td class="bold">AWSALB</td>
				<td>Cookie balanceador de carga AWS que determina a qué servidor se conecta el usuario.</td>
			</tr>
			<tr>
				<td class="bold">XSRF-TOKEN</td>
				<td>Token de seguridad para prevenir ataques de secuencias de comandos maliciosas que se inyectan en sitios web benéficos y seguros. Esta cookie es esencial para la seguridad del sitio web y del visitante.</td>
			</tr>
			<tr>
				<td class="bold">laravel_session</td>
				<td>Utilizada principalmente por los propietarios del sitio web al cargar o renovar el contenido del sitio web.</td>
			</tr>
			<tr>
				<td class="bold">_cfduid</td>
				<td>Utilizada por la red de contenido, Cloudflare, para identificar el tráfico web de confianza.</td>
			</tr>
		</table>

		<br>

		<p>
			<span class="bold">2) Desempeño: </span> En estas cookies se recopila información acerca de cómo los visitantes usan un sitio web, por ejemplo, qué páginas visitan con mayor frecuencia. En estas cookies no se recopila información que identifique al visitante. Toda la información recopilada en estas cookies es agregada, por lo tanto, anónima. Solo se utiliza para mejorar el funcionamiento de un sitio web.
		</p>

		<table class="cookies-table">
			<tr>
				<td class="bold">__utma</td>
				<td>Es utilizada para distinguir usuarios y sesiones. La cookie se actualiza cada vez que data es enviada a Google Analytics.</td>
			</tr>
			<tr>
				<td class="bold">__utmc</td>
				<td>Actúa en conjunto con la __utmb cookie para determinar si el usuario estaba en una nueva sesión o visita, (Google Analytics).</td>
			</tr>
			<tr>
				<td class="bold">__utmb</td>
				<td>Es utilizada para determinar nuevas sesiones o visitas. La cookie se actualiza cada vez que data es enviada a Google Analytics</td>
			</tr>
			<tr>
				<td class="bold">__utmt_UA-86808048-1</td>
				<td>Es utilizada por Google Analytics para restringir la velocidad de solicitudes.</td>
			</tr>
			<tr>
				<td class="bold">__utmz</td>
				<td>Almacena la fuente del tráfico o las campañas que explican al usuario que ha llegado al sitio web. La cookie se actualiza cada vez que data es enviada a Google Analytics</td>
			</tr>
			<tr>
				<td class="bold">_gat</td>
				<td>Es utilizada por Google Analytics para restringir la velocidad de solicitudes.</td>
			</tr>
			<tr>
				<td class="bold">_gid</td>
				<td>Es utilizada para distinguir usuarios, (Google Analytics).</td>
			</tr>
			<tr>
				<td class="bold">collect</td>
				<td>Se utiliza para enviar datos a Google Analytics sobre el dispositivo y el comportamiento del visitante. Sigue al visitante a través de los servicios y canales de comercialización</td>
			</tr>
			<tr>
				<td class="bold">personalization_id</td>
				<td>Está configurada por Twitter, la cookie permite al visitante compartir contenido del sitio web en su perfil de Twitter.</td>
			</tr>
		</table>

		<br>

		<p>
			<span class="bold">3) Funcionalidad: </span> Estas cookies permiten al sitio web recordar selecciones que haya hecho y ofrecer características mejoradas y más personales. Por ejemplo, se pueden utilizar estas cookies para recordar y guardar el número de rastreo más reciente que haya introducido con una aplicación de rastreo. La información que se recopila en estas cookies puede hacerse anónima y no podrán rastrear su actividad de navegación en otros sitios web.
		</p>

		<table class="cookies-table">
			<tr>
				<td class="bold">laravel_session</td>
				<td>Guarda identificador único de la sesión del usuario.</td>
			</tr>
			<tr>
				<td class="bold">collect_chat_closed</td>
				<td>Guarda el número de conversiones cerradas.</td>
			</tr>
			<tr>
				<td class="bold">collect_chat_completed</td>
				<td>Guarda el número de conversiones completadas.</td>
			</tr>
			<tr>
				<td class="bold">lang</td>
				<td>Recuerda la versión de idioma seleccionada por el usuario de un sitio web.</td>
			</tr>
			<tr>
				<td class="bold">lang</td>
				<td>Configurada por LinkedIn cuando una página web contiene un icono de "Síguenos" incrustado.</td>
			</tr>
		</table>

		<br>

		<p>
			<span class="bold">4) Segmentación o publicidad: </span> Estas cookies se utilizan para entregar avisos publicitarios que sean más relevantes para usted y sus intereses. También se utilizan para limitar el número de veces que ve una publicidad, así como ayudar a medir la efectividad de una campaña publicitaria. Generalmente, las redes publicitarias las colocan con el permiso del operador del sitio web.
		</p>

		<table class="cookies-table">
			<tr>
				<td class="bold">__utma</td>
				<td>Es utilizada para distinguir usuarios y sesiones. La cookie se actualiza cada vez que data es enviada a Google Analytics.</td>
			</tr>
			<tr>
				<td class="bold">_gat</td>
				<td>Es utilizada para restringir la velocidad de solicitudes, (Google Analytics).</td>
			</tr>
			<tr>
				<td class="bold">_gid</td>
				<td>Es utilizada para distinguir usuarios, (Google Analytics).</td>
			</tr>
			<tr>
				<td class="bold">_fbp</td>
				<td>Es utilizada por Facebook para ofrecer una serie de productos publicitarios, como ofertas en tiempo real de terceros anunciantes.</td>
			</tr>
			<tr>
				<td class="bold">ads/ga-audiences</td>
				<td>Es utilizada por Google Adwords para volver a atraer a los visitantes que probablemente se conviertan en clientes, en función del comportamiento en línea del visitante en todos los sitios web.</td>
			</tr>
			<tr>
				<td class="bold">bcookie</td>
				<td>Es utilizada por el servicio de redes sociales profesional, Linkedin, para rastrear el uso de servicios integrados.</td>
			</tr>
			<tr>
				<td class="bold">BizoID</td>
				<td>Se utiliza para realizar un seguimiento de los visitantes en varios sitios web, con el fin de presentar publicidad relevante en función de las preferencias del visitante.</td>
			</tr>
			<tr>
				<td class="bold">bscookie</td>
				<td>Es utilizada por el servicio de redes sociales, Linkedin, para rastrear el uso de servicios integrados.</td>
			</tr>
			<tr>
				<td class="bold">fr</td>
				<td>Es utilizada por Facebook para ofrecer una serie de productos publicitarios, como ofertas en tiempo real de terceros anunciantes.</td>
			</tr>
			<tr>
				<td class="bold">i/adsct</td>
				<td>Es utilizada por Twitter.com para determinar el número de visitantes que acceden al sitio web a través del contenido de publicidad de Twitter.</td>
			</tr>
			<tr>
				<td class="bold">lidc</td>
				<td>Es utilizada por el servicio de redes sociales, Linkedin, para rastrear el uso de servicios integrados.</td>
			</tr>
			<tr>
				<td class="bold">tr</td>
				<td>Es utilizada por Facebook para ofrecer una serie de productos publicitarios, como ofertas en tiempo real de terceros anunciantes.</td>
			</tr>
			<tr>
				<td class="bold">UserMatchHistory</td>
				<td>Se utiliza para realizar un seguimiento de los visitantes en varios sitios web, con el fin de presentar publicidad relevante en función de las preferencias del visitante.</td>
			</tr>
		</table>
	</section>

	<section>
		<h3>¿Cómo controlar las cookies?</h3>

		<p>Usted puede controlar o borrar las cookies siempre que lo desee: para más información, consulte <a href="https://www.aboutcookies.org/" target="_blank">aboutcookies.org</a>. Además de poder eliminar todas las cookies que ya se encuentran en su ordenador, también puede configurar la mayoría de los navegadores para que dejen de aceptarlas. Pero tenga presente que, si rechaza las cookies, es posible que tenga que volver a configurar manualmente sus preferencias cada vez que visite un sitio y que dejen de funcionar determinados servicios y funcionalidades.</p>
	</section>

	<section>
		<h3>¿Cómo administrar cookies y data de nuestro sitio web en su explorador?</h3>

		<p>La mayoría de los navegadores aceptan cookies automáticamente, pero los visitantes pueden modificar la configuración de su navegador para borrar las cookies o para evitar la aceptación automática. A través de los siguientes vínculos, puede ir a las páginas de los exploradores y aplicaciones comúnmente utilizadas para aprender acerca de cómo administrar cookies y datos desde sitios Web hacia su dispositivo.</p>

		<p>
			<span class="bold">Chrome:</span>
			<a href="https://support.google.com/chrome/answer/95647?hl=en">https://support.google.com/chrome/answer/95647?hl=en</a>
		</p>

		<p>
			<span class="bold">Firefox:</span>
			<a href="https://www.mozilla.org/en-US/privacy/websites/#cookies">https://www.mozilla.org/en-US/privacy/websites/#cookies</a>
		</p>

		<p>
			<span class="bold">Internet Explorer:</span>
			<a href="https://privacy.microsoft.com/en-us/privacystatement">https://privacy.microsoft.com/en-us/privacystatement</a>
		</p>

		<p>
			<span class="bold">Safari:</span>
			<a href="https://support.microsoft.com/en-us/hub/4338813/windows-help">https://support.microsoft.com/en-us/hub/4338813/windows-help</a>
		</p>

		<p>
			<span class="bold">Opera:</span>
			<a href="https://help.opera.com/en/latest/security-and-privacy/#tracking">https://help.opera.com/en/latest/security-and-privacy/#tracking</a>
		</p>
	</section>

	<section>
		<h3>Consentimiento a las cookies</h3>

		<p>Al continuar utilizando el sitio web sin modificar sus preferencias o aceptar/rechazar nuestra utilización de cookies, se entiende que usted acepta la colocación de cookies en su dispositivo. Si elige no recibir cookies, no podemos garantizar que su experiencia sea tan satisfactoria como lo sería de otra manera ya que la mayoría de las cookies que utilizamos son esenciales para su navegación.</p>

		<p>Para aceptar o rechazar las cookies de esta web, solo tiene que hacer clic en el enlace correspondiente:</p>

		<div>
            <a href="'. route('welcome') .'" class="btn btn-orange btn-cookies-accept">Acepto las cookies</a>
            <a id="btn-terms-reject" href="'. route('cookies.reject') .'" class="terms-link">Rechazo las cookies</a>
        </div>
	</section>',

	'reject-text' => '<div class="home-text">
	<p>Hola,</p>
	
	<p>Gracias por visitar nuestra página web, la mayoría de las cookies que utilizamos son esenciales para su navegación óptima. Al no aceptar nuestra configuración de cookies no podemos  mantener tu acceso a navegarla.</p>

	<p>Para mayor información sobre nuestra utilización de cookies, el tipo de datos que recabamos e instrucciones para manejarlas y borrarlas de tu dispositivo por favor visita nuestra <a href="'. route('cookies.declaration') .'" class="terms-link">Declaración de Cookies</a>.</p>

	<p>Las cookies que usamos nos permiten darte un servicio de primera, ¿Deseas continuar?.</p>

	<br>
	</div>

	<div class="terms-notice-button">
		<div class="col-md-6 text-center">
        	<a href="'. route('welcome') .'" class="btn btn-orange btn-cookies-accept">Volver a página web y aceptar cookies</a>
        </div>
        <div class="col-md-6 text-center">
        	<a href="http://www.google.com" class="terms-link">De vuelta a Google</a>
    	</div>
    </div>',
];