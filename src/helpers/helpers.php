<?php

use Illuminate\Support\Facades\Log;

use App\PixelsAndCookies\Acceptation;
use App\PixelsAndCookies\Pixel;

function getPixel($url, $type, $locale = null)
{
    try
    {

        if(!empty($locale))
        {
           $pixels = Pixel::ofUrl($url)->where([
                ['type','=',$type],
                ['locale','=',$locale],
                ['requires_acceptance','=',false]
            ])->get();
        }
        else
        {
           $pixels = Pixel::ofUrl($url)->where([
                ['type','=',$type],
                ['requires_acceptance','=',false]
            ])->get();
        }

        $metaTag = view('pixels::html', compact('pixels'))->render();

        return $metaTag;

    } catch (Exception $e) {
        Log::info('Exception generada por la función getPixel del paquete pixels and cookies package');
    }
}

function checkTermsAcceptationFor($userSession)
{
    $acceptation = Acceptation::where('session','=',$userSession)->first();

    return $acceptation;
}

function getSessionId()
{
    return \Request::cookie(config('session.cookie'));
}