<?php

namespace App\PixelsAndCookies;

use Illuminate\Database\Eloquent\Model;

class CookieType extends Model
{
    public function pixels()
    {
    	return $this->hasMany('App\PixelsAndCookies\Pixel');
    }

    public function acceptations()
    {
    	return $this->belongsToMany('App\PixelsAndCookies\Acceptation');
    }
}

