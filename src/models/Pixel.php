<?php

namespace App\PixelsAndCookies;

use Illuminate\Database\Eloquent\Model;

class Pixel extends Model
{
    protected $table = 'pixels';

    public function cookietype()
    {
    	return $this->belongsTo('App\PixelsAndCookies\CookieType');
    }

    public function scopeOfUrl($query, $url)
    {
        return $query->where('url', $url);
    }
}
