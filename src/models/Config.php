<?php

namespace App\PixelsAndCookies;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'pixels_and_cookies_configs';

}
