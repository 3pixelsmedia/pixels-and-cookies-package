<?php

namespace App\PixelsAndCookies;

use Illuminate\Database\Eloquent\Model;

class Acceptation extends Model
{
    public function cookietypes()
    {
    	return $this->belongsToMany('App\PixelsAndCookies\CookieType','acceptation_cookietype');
    }
}
