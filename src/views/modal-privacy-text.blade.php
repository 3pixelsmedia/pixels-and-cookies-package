@if(empty(checkTermsAcceptationFor(getSessionId())) && App\PixelsAndCookies\Config::where('var','=','gdpr_cookies')->first()->value == 'active')

  <link href="{{ URL::asset('vendor/pixels-and-cookies-theme/pixels-and-cookies.css') }}" rel="stylesheet">

  <form id="cookie-type-form" class="@if(Route::currentRouteName() == 'cookies.reject' || Route::currentRouteName() == 'cookies.declaration') hide @endif">
    <div id="terms-notice" class="col-xs-12 col-md-4">
      <div class="col-xs-12">
           {!! __('pixels-and-cookies.text') !!}
        <div class="hide" id="gdprcookie-types">
           {!! __('pixels-and-cookies.customise-section-title') !!}
          <ul id="cookie-type-list">
            @foreach($allCookieTypes as $index => $cookieType)
              <li class="cookie-type col-xs-6">
                <input type="checkbox" id="cookie-type-{{ ($index) }}" name="cookie-type[]" value="{{ $cookieType->js_function_name }}" @if($cookieType->checked) checked="checked" @endif @if($cookieType->disabled_input) disabled="disabled" @endif>
                <label for="cookie-type-{{ ($index) }}" title="@if(App::isLocale('en')) {{ $cookieType->name_en }} @elseif(App::isLocale('es')) {{ $cookieType->name_es }} @else {{ $cookieType->name_pt }} @endif">@if(App::isLocale('en')) {{ $cookieType->name_en }} @elseif(App::isLocale('es')) {{ $cookieType->name_es }} @else {{ $cookieType->name_pt }} @endif</label>

                 @if($cookieType->disabled_input)
                  <input type="hidden" name="cookie-type[]" value="{{ $cookieType->js_function_name }}">
                 @endif

              </li> 
            @endforeach
          </ul>
        </div>
        <div class="clear"></div>
        <div class="terms-notice-button">
            <button id="btn-terms-ok" class="btn-primary" value="{{ getSessionId() }}">{{ __('pixels-and-cookies.btn-accept') }}</button>
            <button id="btn-terms-custom" class="btn-primary">{{ __('pixels-and-cookies.btn-customise') }}</button>
        </div>
      </div>
    </div>
  </form>

  @php            
     \Cookie::queue(\Cookie::forget('XSRF-TOKEN'));
     \Cookie::queue(\Cookie::forget('laravel_session'));
  @endphp

  <script type="text/javascript">
      disableCookies();
  </script>

@else
  <script type="text/javascript">
    enableCookies();
    @foreach($allCookieTypes as $cookieType)
      window[{!! $cookieType->js_function_name.'()' !!}];
    @endforeach
  </script>
@endif
