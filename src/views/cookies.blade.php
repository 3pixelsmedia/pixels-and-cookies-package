@php

	if(empty(getSessionId()))
	{
		$allCookieTypes = \App\PixelsAndCookies\CookieType::has('pixels')->orWhere('disabled_input','=',true)->get();
	}
   	else
   	{
   		$userSessionId 	= getSessionId();
   		
   		if(empty(\App\PixelsAndCookies\Acceptation::where('session','=',$userSessionId)->first()))
   		{
   			$allCookieTypes = \App\PixelsAndCookies\CookieType::has('pixels')->orWhere('disabled_input','=',true)->get();
   		}
   		else
   		{
   			$userCookies   	= \App\PixelsAndCookies\Acceptation::where('session','=',$userSessionId)->first()->cookietypes()->get();
   			$allCookieTypes = $userCookies;
   		}
   	}

@endphp

<link href="{{ URL::asset('css/app.css') }}" rel="stylesheet">
<link href="{{ URL::asset('vendor/pixels-and-cookies-theme/pixels-and-cookies.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>

<script type="text/javascript">

@foreach($allCookieTypes as $cookieType)
	{!! 'function '.$cookieType->js_function_name.'(){' !!}
	
		@foreach($cookieType->pixels as $pixel)
			@if(($cookieType->id == $pixel->cookie_type_id && $pixel->locale == \App::getLocale()) || $cookieType->id == $pixel->cookie_type_id && $pixel->locale == NULL)
				{!! $pixel->html !!}
			@endif
		@endforeach

	{!! '}' !!}
@endforeach

function enableCookies()
{
	$("input[name='cookie-type[]']:checked").each(function ()
	{
		window[$(this).val()]();
	});

	enableElementsInDOM();

	$("#terms-notice").remove();
}

function disableCookies()
{
	disableElementsInDOM();
}

function hasCookies()
{
	if(document.cookie.length == 0)
		return false;
	else
		return true;
}


$(document).ready(function(){

	if($("#btn-terms-ok").length > 0)
	{
		$("#btn-terms-ok").click(function(e){

			e.preventDefault();
			saveAcceptation();

	  	});
	}

	if($("#btn-terms-custom").length > 0)
	{
		$("#btn-terms-custom").click(function(e){
			e.preventDefault();
			$("#gdprcookie-types").removeClass('hide');
		});
	}

	if(hasCookies() === false)
	{
		disableElementsInDOM();
	}
	
	var isAjaxCalled = false;

	$(window).scroll(function(){

		if($(".cookies-description-container").length > 0)
			return;

		if($(window).scrollTop() > 300)
		{
			if($("#btn-terms-ok").length > 0 && !isAjaxCalled)
			{
				isAjaxCalled = true;
				saveAcceptation();	
			}
			else
			{
				if(hasCookies() === false && !isAjaxCalled)
					enableCookies();
			}
		}
	});

	$("a").not($('.terms-link')).click(function(e){
		e.preventDefault();
		
		if($(this).attr('id') == 'btn-more-articles')
		{
			getMoreArticles();
		}
		else
		{
			var url = $(this).attr('href');
		
			if($(this).hasClass('terms-link') === false && hasCookies() === false)
			{
				saveAcceptation(url);
			}
			else
			{
				window.location = url;
			}
		}
	});
});


function enableElementsInDOM()
{
	if($("#btn-video").length > 0)
	{
		$("#btn-video").removeAttr('disabled');
	}

	if($("#plans-container").length > 0)
	{
		if($(".plan-item").length == 0)
		{
			getPlansToShow(localStorage.getItem('consultantCode'));
			$("#loading-container").removeClass('hide');
			$("#plans-container").removeClass('hide');
		}
	}
}

function disableElementsInDOM()
{
	if($("#plans-container").length > 0)
	{
		$("#loading-container").addClass('hide');
		$("#plans-container").addClass('hide');
	}

	if($("#btn-video").length > 0)
	{
		$("#btn-video").attr('disabled','disabled');
	}
}

function saveAcceptation(url = null)
{
	$.get('/refresh-csrf').done(function(data){
        csrfToken = data; // the new token

	    $.ajax({
	    method: "POST",
	    data: $("#cookie-type-form").serialize(),
	      url: "/save-acceptation",
	      headers: {
	          'X-CSRF-TOKEN': csrfToken
	      }
	    }).done(function( data ) {

	    	enableCookies();
	      	$("#terms-notice").remove();

	      	if(url != null)
	      		window.location = url;

	    }).fail(function(dataError) {

	      var obj = dataError.responseJSON; 
	      
	    });
	});
}

</script>

@include('pixels-and-cookies-package::modal-privacy-text')


