<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PixelsAndCookies\Acceptation;
use App\PixelsAndCookies\CookieType;

class AcceptationCookiesController extends Controller
{
    public function save(Request $request)
    {
        $data         = $request->all();
        $userSession  = getSessionId();
        $acceptation  = checkTermsAcceptationFor($userSession);

        if(empty($acceptation))
        {
            $acceptation = new Acceptation();
                $acceptation->session = $userSession;
            $acceptation->save();

            foreach ($data['cookie-type'] as $cookieType) 
            {
                $cookieType = CookieType::where('js_function_name','=',$cookieType)->first();
                $acceptation->cookietypes()->save($cookieType);
            }
        }
    }
}
