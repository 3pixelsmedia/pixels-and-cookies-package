<?php

use Illuminate\Database\Seeder;

class PixelsAndCookiesConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pixels_and_cookies_configs')->insert([  
            0=>[  
                'var'         => 'gdpr_cookies',        
                'value'       => 'active',
                'description' => 'GDPR Cookies package (active or inactive)',
            ]
        ]);
    }
}