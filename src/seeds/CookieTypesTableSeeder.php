<?php

use Illuminate\Database\Seeder;

class CookieTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cookie_types')->insert([  
            0=>[  
                'name_en'           =>'Essential',        
                'name_es'           =>'Esencial',
                'name_pt'           =>'Essencial',
                'checked'           =>true,
                'disabled_input'    =>true,
                'js_function_name'  =>'essentialCookies'
            ],
            1=>[  
                'name_en'           =>'Site Preferences',        
                'name_es'           =>'Preferencias del sitio',
                'name_pt'           =>'Preferências do site',
                'checked'           =>true,
                'disabled_input'    =>false,
                'js_function_name'  =>'sitePreferencesCookies'
            ],
            2=>[  
                'name_en'           =>'Analytics',        
                'name_es'           =>'Analítica',
                'name_pt'           =>'Analytics',
                'checked'           =>true,
                'disabled_input'    =>false,
                'js_function_name'  =>'analyticsCookies'
            ],
            3=>[  
                'name_en'           =>'Marketing',        
                'name_es'           =>'Marketing',
                'name_pt'           =>'Marketing',
                'checked'           =>true,
                'disabled_input'    =>false,
                'js_function_name'  =>'marketingCookies'
            ]
        ]);
    }
}