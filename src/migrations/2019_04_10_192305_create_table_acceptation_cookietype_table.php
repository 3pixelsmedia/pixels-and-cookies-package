<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAcceptationCookietypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acceptation_cookietype', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('acceptation_id')->unsigned();
            $table->integer('cookie_type_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('acceptation_cookietype', function (Blueprint $table) {
            $table->foreign('acceptation_id')->references('id')->on('acceptations')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('acceptation_cookietype', function (Blueprint $table) {
            $table->foreign('cookie_type_id')->references('id')->on('cookie_types')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acceptation_cookietype');
    }
}
