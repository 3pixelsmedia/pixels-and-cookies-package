<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCookieTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cookie_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en');
            $table->string('name_es');
            $table->string('name_pt');
            $table->text('description_en')->nullable();
            $table->text('description_es')->nullable();
            $table->text('description_pt')->nullable();
            $table->boolean('checked')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cookie_types');
    }
}
