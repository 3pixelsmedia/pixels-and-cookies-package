<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCookieTypeForeignKeyToPixelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pixels', function (Blueprint $table) {
            $table->integer('cookie_type_id')->unsigned()->nullable();
        });

        Schema::table('pixels', function (Blueprint $table) {
            $table->foreign('cookie_type_id')->references('id')->on('cookie_types')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pixels', function (Blueprint $table) {
            $table->dropForeign('pixels_cookie_type_id_foreign');
            $table->dropIndex('pixels_cookie_type_id_foreign');
            $table->dropColumn('cookie_type_id');
        });
    }
}
