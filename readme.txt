PASOS PARA INTEGRAR EL PAQUETE A UN PROYECTO LARAVEL


1. Agrega el siguiente repositorio al archivo composer.json del proyecto

	"repositories":[
	    {
	        "type":"vcs",
	        "url":"https://bitbucket.org/3pixelsmedia/pixels-and-cookies-package.git"
	    }
	]


2. Ejecuta por consola: composer require 3pixelsmedia/pixels-and-cookies-package:dev-master


3. Agrega la siguiente clase al array "providers" en el archivo config / app.php
	
	Trespixelsmedia\PixelsAndCookies\PixelsAndCookiesServiceProvider::class,


4. Ejecuta los siguientes comando desde el terminal en el directorio raíz del proyecto:

	composer dump-autoload

	php artisan vendor:publish --tag=pixels-and-cookies-essential-files (Publica los archivos esenciales del paquete)

	Opcional (Publica las migraciones del paquete en el directorio "migrations/pixels-and-cookies"):
	php artisan vendor:publish --tag=pixels-and-cookies-migrations

	Opcional (Publica las vistas del paquete):
	php artisan vendor:publish --tag=pixels-and-cookies-views


5. Corre las migraciones

	php artisan migrate


6. Ejecutar composer dump-autoload


7. Corre los seeds

	php artisan db:seed --class=CookieTypesTableSeeder

	php artisan db:seed --class=PixelsAndCookiesConfigsTableSeeder


8. Agregar el helper al "autoload del composer.json del proyecto:
	 
	"files": [
            "app/Helpers/PixelsAndCookies/helpers.php"
    ]

9. Agregar las siguientes rutas al archivo web de rutas:

	Route::post('/save-acceptation','AcceptationCookiesController@save');

	Route::get('refresh-csrf', function(){
		cookie('XSRF-TOKEN', Crypt::encrypt(csrf_token()));
	   	return csrf_token();
	});

	Route::get('/cookies-declaracion', function(){
		return view('vendor.pixels-and-cookies-package.cookies-declaration');
	})->name('cookies.declaration');

	Route::get('/cookies-rechazo', function(){
		return view('vendor.pixels-and-cookies-package.cookies-reject');
	})->name('cookies.reject');


10. Ejecutar composer dump-autoload


11. Incluir la llamada al paquete en el body del proyecto:
	
	@include('pixels-and-cookies-package::cookies') 

12. Agrega los script que contengan cookies a la tabla pixels, indicando a que tipo de cookies pertenece y setea a true el campo "requires_acceptance", por ejemplo:

	Google Tag Manager: Tipo de cookie es Analytics (cookie_type_id = 3)
	id, type, url, html, created_at, updated_at, locale, requires_acceptance, cookie_type_id

	1, head, all, (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','XXXXXXX');, , , , 1, 3


